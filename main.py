#!/bin/python3

from numpy import imag
from imagepointer import ImagePointer
from tkinter import messagebox

ip = ImagePointer()

try:
    ip.run()
except Exception as e:
    messagebox.showerror("Error", str(e))
    exit(-1)
