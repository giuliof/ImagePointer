import cv2
import numpy as np

'''
Acquires frames from camera, then adds a red cross as overlay.
With mouse double clicks, cross can be moved
'''

class ImagePointer:
    center_coord : np.ndarray
    center_size : int
    center_thickness : int
    canvas_name : str

    def __init__(self):
        # Cross coordinates
        self.center_coord = np.array((0,0))
        self.center_size = 10
        self.center_thickness = 3
        self.canvas_name = 'Image Pointer'
        self.rotate = False

    # OpenCV callbacks
    def on_mouse_event(self, event, x, y, flags, param):
        # if event == cv2.EVENT_LBUTTONDBLCLK:
        if event == cv2.EVENT_LBUTTONUP:
            # Update cross position
            self.center_coord = np.array((x, y))

    def on_center_size_change(self, value):
        if value > 0:
            self.center_size = value

    def on_center_thickness_change(self, value):
        if value > 0:
            self.center_thickness = value

    def on_rotate_changed(self, value):
        self.rotate = value == 1

    def run(self):
        # Camera feed
        cam = cv2.VideoCapture(0)
        if not cam.isOpened():
            raise Exception('Cannot open camera')
        ret, frame_cam = cam.read()
        if not ret:
            cam.release()
            raise Exception('Cannot open camera stream')

        # Put cross in image center as default
        self.center_coord = np.array((int(cam.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cam.get(cv2.CAP_PROP_FRAME_HEIGHT))))
        self.center_coord //= 2

        cv2.namedWindow(self.canvas_name, cv2.WND_PROP_FULLSCREEN)
        cv2.setMouseCallback(self.canvas_name, self.on_mouse_event)

        cv2.createTrackbar('center size', self.canvas_name, self.center_size, 100, self.on_center_size_change)
        cv2.createTrackbar('center thickness', self.canvas_name, self.center_thickness, 16, self.on_center_thickness_change)
        cv2.createTrackbar('rotate', self.canvas_name, 0, 1, self.on_rotate_changed)

        while True:
            # Capture the next frame from camera
            ret, frame_cam = cam.read()
            if not ret:
                print('Cannot receive frame from camera')
                break
            
            # Acquire frame
            frame = frame_cam.astype(np.uint8)

            # Rotate if needed
            if self.rotate:
                frame = cv2.rotate(frame, cv2.ROTATE_180)
            
            ## Add cross
            frame = cv2.line(frame, self.center_coord - (0, self.center_size), self.center_coord + (0, self.center_size), (0, 0, 255,), self.center_thickness)
            frame = cv2.line(frame, self.center_coord - (self.center_size, 0), self.center_coord + (self.center_size, 0), (0, 0, 255,), self.center_thickness)

            cv2.imshow(self.canvas_name, frame)

            # detect ESC key or if window is closeed 
            if cv2.waitKey(1) == 27 or cv2.getWindowProperty(self.canvas_name,cv2.WND_PROP_VISIBLE) < 1:  
                break

        cam.release()
        cv2.destroyAllWindows()
